// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2023 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau.hh>
#include <cstdlib>
#include <iostream>
#include <map>
#include <random>
#include <set>

#include "tetcolor.hh"
#include "tetcolor.ico.hh"
#include "left.ico.hh"
#include "right.ico.hh"
#include "down.ico.hh"
#include "rot.ico.hh"
#include "stop.ico.hh"
#include "pause.ico.hh"
#include "exit.ico.hh"
#include "flaticon.ico.hh"

tau::Key_file           kstate_;
const std::string_view  colors_[6] { "Red", "Lime", "DodgerBlue", "Yellow", "Magenta", "White" };

const int HMAX = 18;
const int WMAX = 7;
const int YMAX = HMAX-1;
const int XMAX = WMAX-1;

struct Main: public tau::Toplevel {
    using Ys = std::set<int>;
    using Pool = std::map<int, Ys>;

    Pool                    pool_;
    int                     level_ = 0;
    int                     score_ = 0;
    int                     bonus_ = 0;
    int                     nbonus_ = 0;
    int                     h_branches_ = 0;
    int                     v_branches_ = 0;
    int                     d_branches_ = 0;
    int                     chrome_[HMAX][WMAX];
    tau::Action             left_action_    { "Left 4", fun(this, &Main::on_left) };
    tau::Action             right_action_   { "Right 6", fun(this, &Main::on_right) };
    tau::Action             drop_action_    { "Down 2 Space", fun(this, &Main::on_drop) };
    tau::Action             rot_action_     { "Up 8 5", fun(this, &Main::on_rot) };
    tau::Action             pause_action_   { tau::KC_PAUSE, tau::KM_NONE, fun(this, &Main::on_pause) };
    tau::Action             escape_action_  { "Escape Cancel", fun(this, &Main::on_escape) };
    tau::Widget             boxes_[HMAX][WMAX];
    tau::Scroller           scroller30_;
    tau::Table              table30_        { 10, 0 };
    tau::Counter            index30_[30];
    tau::Entry              name30_[30];
    tau::Counter            score30_[30];
    tau::Table              table_ { 1 };
    tau::Box                box3_           { tau::Orientation::WEST };
    tau::Label              banner_;
    tau::Label              level_label_    { "LEVEL" };
    tau::Counter            level_value_    { tau::Border::NONE };
    tau::Label              score_label_    { "SCORE" };
    tau::Counter            score_value_    { tau::Border::NONE };
    tau::Counter            bonus_value_    { tau::Border::NONE };
    tau::Button             reset_button_   { "CLEAR TOP 30" };
    tau::Frame              enter_          { tau::Border::NONE };
    std::string_view        mt_             { "#aeaeae" };
    tau::Timer              level_timer_    { fun(this, &Main::on_level_timer) };
    tau::Timer              tick_timer_     { fun(this, &Main::on_tick_timer) };
    tau::Timer              drop_timer_     { fun(this, &Main::on_drop_timer) };
    tau::Timer              pool_timer_     { fun(this, &Main::on_pool_timer) };
    tau::Timer              bonus_timer_    { fun(this, &Main::on_bonus_timer) };
    tau::connection         key_cx_;
    tau::connection         pause_cx_;
    tau::connection         down_cx_;
    tau::connection         wheel_cx_;
    tau::connection         blink_cx_;
    tau::Point              fig_[3]         { tau::Point(-1, -1), tau::Point(-1, -1), tau::Point(-1, -1) };
    std::random_device      rdev_;
    std::mt19937            rng_            { rdev_() };

    Main(const tau::ustring & title, const tau::Rect & bounds=tau::Rect()):
        tau::Toplevel(title, bounds)
    {
        try { set_icon(tau::Pixmap::create(tetcolor_ico, tetcolor_ico_len)); }
        catch (tau::bad_pixmap & x) { std::cerr << "** " << x.what() << std::endl; }

        score_value_.disallow_edit(), score_value_.hide_buttons();
        score_value_.conf().set(tau::Conf::FOREGROUND, "Yellow");
        level_value_.disallow_edit(), level_value_.hide_buttons();
        bonus_value_.conf().set(tau::Conf::FOREGROUND, "OrangeRed");
        bonus_value_.disallow_edit(), bonus_value_.hide_buttons();

        conf().set(tau::Conf::BACKGROUND, "#222");
        conf().set(tau::Conf::WHITESPACE_BACKGROUND, "#222");
        insert(box3_);

        tau::Box box30(tau::Orientation::SOUTH);
        box3_.append(box30);

        tau::Label top30("Top 30");
        top30.hint_margin_bottom(5);
        top30.conf().font().enlarge(8);
        top30.conf().set(tau::Conf::FOREGROUND, "White");
        box30.append(top30, true);

        box30.append(reset_button_, true);
        reset_button_.conf().set(tau::Conf::BUTTON_BACKGROUND, "#B31829");
        reset_button_.conf().set(tau::Conf::FOREGROUND, "#F2DA37");
        reset_button_.hint_margin(0, 4, 4, 6);
        reset_button_.connect(fun(this, &Main::on_reset));
        reset_button_.hide();

        table30_.hint_margin(4);
        auto f = [](int, int, const tau::Point &) { return true; };
        table30_.signal_hints_changed().connect(fun(this, &Main::on_top_hints));
        box30.append(scroller30_);
        scroller30_.insert(table30_);

        enter_.conf().set(tau::Conf::BACKGROUND, "#77A08C");
        enter_.conf().set(tau::Conf::FOREGROUND, "Lime");
        enter_.hint_margin_top(8);
        enter_.hide();
        box30.append(enter_, true);

        tau::Label label("Enter your name or\npress <Escape> to cancel");
        label.hint_margin(4);
        enter_.insert(label);

        for (int i = 0; i < 30; ++i) {
            tau::Key_section & sect = kstate_.section(tau::str_format(i));
            index30_[i].hide_buttons();
            index30_[i].set_border_style(tau::Border::NONE);
            index30_[i].disallow_edit();
            index30_[i].assign(1+i);
            table30_.put(index30_[i], -1, i, 1, 1, true, true);
            name30_[i].signal_approve().connect(tau::fun(tau::str_not_empty));
            name30_[i].set_border_style(tau::Border::NONE);
            name30_[i].disallow_edit();
            name30_[i].wrap(tau::Wrap::ELLIPSIZE_CENTER);
            name30_[i].hint_max_size(200, 0);
            name30_[i].assign(kstate_.get_string(sect, "name", tau::login_name()));
            name30_[i].action_cancel().connect(bind_back(fun(this, &Main::on_name_cancel), i));
            name30_[i].signal_activate().connect(bind_back(fun(this, &Main::on_name_activate), i));
            name30_[i].signal_select().connect(bind_back(fun(this, &Main::on_table30_selection), i));
            name30_[i].signal_mouse_down().connect(fun(std::function<bool(int, int, const tau::Point &)>(f)), true);
            table30_.put(name30_[i], 0, i);
            score30_[i].hide_buttons();
            score30_[i].set_border_style(tau::Border::NONE);
            score30_[i].disallow_edit();
            score30_[i].set_max_value(999999);
            score30_[i].assign(kstate_.get_integer(sect, "score"));
            table30_.put(score30_[i], 1, i, 1, 1, true);
        }

        index30_[0].conf().set(tau::Conf::FOREGROUND, "Red");
        name30_[0].conf().set(tau::Conf::FOREGROUND, "Red");
        score30_[0].conf().set(tau::Conf::FOREGROUND, "Red");

        index30_[1].conf().set(tau::Conf::FOREGROUND, "Yellow");
        name30_[1].conf().set(tau::Conf::FOREGROUND, "Yellow");
        score30_[1].conf().set(tau::Conf::FOREGROUND, "Yellow");

        index30_[2].conf().set(tau::Conf::FOREGROUND, "Yellow");
        name30_[2].conf().set(tau::Conf::FOREGROUND, "Yellow");
        score30_[2].conf().set(tau::Conf::FOREGROUND, "Yellow");

        for (int i = 3; i <= 9; ++i) {
            index30_[i].conf().set(tau::Conf::FOREGROUND, "Lime");
            name30_[i].conf().set(tau::Conf::FOREGROUND, "Lime");
            score30_[i].conf().set(tau::Conf::FOREGROUND, "Lime");
        }

        for (int i = 10; i <= 19; ++i) {
            index30_[i].conf().set(tau::Conf::FOREGROUND, "Cyan");
            name30_[i].conf().set(tau::Conf::FOREGROUND, "Cyan");
            score30_[i].conf().set(tau::Conf::FOREGROUND, "Cyan");
        }

        for (int i = 20; i < 30; ++i) {
            index30_[i].conf().set(tau::Conf::FOREGROUND, "DodgerBlue");
            name30_[i].conf().set(tau::Conf::FOREGROUND, "DodgerBlue");
            score30_[i].conf().set(tau::Conf::FOREGROUND, "DodgerBlue");
        }

        tau::Frame f1(tau::Border::SOLID, 2), f2(tau::Border::SOLID, 1);
        f1.hint_margin(5);
        f1.set_border_color(tau::Color("White"));
        box3_.append(f1, true);
        f1.conf().set(tau::Conf::BACKGROUND, mt_);
        table_.hint_margin(1);
        table_.set_row_margin(0, 1, 0);
        table_.set_column_margin(0, 1, 0);
        table_.set_row_margin(HMAX-1, 0, 1);
        table_.set_column_margin(WMAX-1, 0, 1);
        table_.signal_paint().connect(fun(this, &Main::on_table_paint));
        f1.insert(table_);

        for (int y = 0; y < HMAX; ++y) {
            for (int x = 0; x < WMAX; ++x) {
                chrome_[y][x] = -1;
                boxes_[y][x].hint_min_size(16);
                boxes_[y][x].signal_paint().connect(bind_back(fun(this, &Main::on_box_paint), y, x));
                table_.put(boxes_[y][x], x, y, 1, 1, true, true);
                clear(y, x);
            }
        }

        tau::Box box1 { tau::Align::CENTER };
        box3_.append(box1);
        tau::Table greetings(8);
        greetings.hint_margin(5);
        box1.append(greetings, true);
        tau::Box cbox { tau::Align::CENTER };
        greetings.put(cbox, 0, 0, 3, 1, false, true);
        int ci = 0;
        const tau::ustring ts { tau::str_format(TITLE, ' ', MAJOR, '.', MINOR) };

        for (std::size_t i = 0; i < ts.size(); ++i) {
            tau::Text txt(tau::ustring(1, ts[i]));
            txt.conf().color(tau::Conf::FOREGROUND) = colors_[ci++];
            txt.conf().font().resize(14);
            if (ci >= sizeof colors_/sizeof *colors_) { ci = 0; }
            cbox.append(txt, true);
        }

        tau::Text copyright("© Konstantin Shmelkov 2023,2024\n"
                            "E-mail: mrcashe@gmail.com\n"
                            "The original version was written by\n"
                            "Sergey Sotnikov for MS-DOS in 1991.\n\n"
                            "This is free software distributed under\n"
                            "the terms of the 2 Clause BSD license."/*, tau::Align::FILL*/);
        copyright.conf().set(tau::Conf::FOREGROUND, "#2ECB7C");
        greetings.put(copyright, 0, 100, 3, 1, true, true);

        label = tau::Label("CONTROLS");
        label.hint_margin(8);
        label.conf().font().resize(16);
        label.conf().font().make_bold();
        const tau::ustring controls_color("SkyBlue");
        label.conf().set(tau::Conf::FOREGROUND, controls_color);
        greetings.put(label, 0, 199, 3, 1, false, true);

        try {
            tau::Image img(tau::Pixmap::create(left_ico, left_ico_len), true);
            img.set_tooltip("Move Figure Left");
            greetings.put(img, 0, 200, 1, 1, true, true);
            label = tau::Label("LEFT, 4");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 200, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(right_ico, right_ico_len), true);
            img.set_tooltip("Move Figure Right");
            greetings.put(img, 0, 201, 1, 1, true, true);
            label = tau::Label("RIGHT, 6");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 201, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(down_ico, down_ico_len), true);
            img.set_tooltip("Drop Figure Down");
            greetings.put(img, 0, 202, 1, 1, true, true);
            label = tau::Label("DOWN, SPACE, 2");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 202, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(rot_ico, rot_ico_len), true);
            img.set_tooltip("Rotate Figure");
            greetings.put(img, 0, 203, 1, 1, true, true);
            label = tau::Label("UP, 8, 5");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 203, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(pause_ico, pause_ico_len), true);
            img.set_tooltip("Pause Game");
            greetings.put(img, 0, 204, 1, 1, true, true);
            label = tau::Label("PAUSE");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 204, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(stop_ico, stop_ico_len), true);
            img.set_tooltip("Cancel Game");
            greetings.put(img, 0, 205, 1, 1, true, true);
            label = tau::Label("ESCAPE");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::FILL, 1, 205, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        try {
            tau::Image img(tau::Pixmap::create(exit_ico, exit_ico_len), true);
            img.set_tooltip("Quit Game");
            greetings.put(img, 0, 206, 1, 1, true, true);
            label = tau::Label("ESCAPE, ALT-F4");
            label.conf().set(tau::Conf::FOREGROUND, controls_color);
            label.conf().font().resize(16);
            label.conf().font().make_bold();
            greetings.put(label, tau::Align::START, tau::Align::CENTER, 1, 206, 2, 1, true, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        tau::Box box(tau::Orientation::EAST, 4);
        greetings.set_row_margin(399, 6, 0);
        greetings.put(box, 0, 399, 3, 1, false, true);

        try {
            tau::Image img(tau::Pixmap::create(flaticon_ico, flaticon_ico_len), true);
            img.hint_margin(1, 1, 0, 0);
            box.append(img, true);
        }

        catch (tau::bad_pixmap & x) {
            std::cerr << "** " << x.what() << std::endl;
        }

        tau::Frame frame;
        box.append(frame);

        label = tau::Text("Icons made by https://www.flaticon.com");
        label.hint_margin(4, 4, 0, 0);
        label.conf().font().enlarge(-2);
        label.conf().set(tau::Conf::BACKGROUND, "#4ad295");
        frame.insert(label);

        box = tau::Box(tau::Orientation::DOWN);
        box.conf().set(tau::Conf::FOREGROUND, "#4ad295");
        box.conf().font().enlarge(-3);
        greetings.put(box, 1, 401, 2, 1, false, true);

        for (auto & s: tau::str_explode("Rizki Ahmad Fauzi:Iconpro86:Slidicon:Freepik", ':')) {
            label = tau::Text("- "+s, tau::Align::START);
            box.append(label, true);
        }

        table_.signal_size_changed().connect(fun(this, &Main::on_table_size));
        signal_position_changed().connect(fun(this, &Main::on_geometry_changed));
        signal_size_changed().connect(fun(this, &Main::on_geometry_changed));
        signal_minimize().connect(fun(pause_action_, &tau::Action::exec));
        key_cx_ = signal_key_down().connect(fun(this, &Main::on_key_down));
        pause_cx_ = signal_key_down().connect(fun(this, &Main::on_key_pause));
        pause_cx_.block();
        connect_action(escape_action_);
        connect_action(left_action_);
        connect_action(right_action_);
        connect_action(drop_action_);
        connect_action(rot_action_);
        connect_action(pause_action_);
        left_action_.disable();
        right_action_.disable();
        drop_action_.disable();
        rot_action_.disable();
        pause_action_.disable();

        down_cx_ = signal_mouse_down().connect(fun(this, &Main::on_mouse_down)); down_cx_.block();
        wheel_cx_ = signal_mouse_wheel().connect(fun(this, &Main::on_mouse_wheel)); wheel_cx_.block();
    }

    void on_table30_selection(int place) {
        tau::ustring c;
        if (0 == place) { c = "Gold"; }
        else if (place < 3) { c = "Red"; }
        else if (place < 10) { c = "DarkOrange"; }
        else if (place < 20) { c = "Blue"; }
        else { c = "LightGray"; }
        table30_.conf().set(tau::Conf::SELECT_BACKGROUND, c);
    }

    void score(int n) {
        if (n > 0) {
            score_ += n;
            score_value_.assign(score_);

            for (int i = 0; i < 30; ++i) {
                if (score_ >= kstate_.get_integer(kstate_.section(tau::str_format(i)), "score")) {
                    table30_.select({ INT_MIN, i, INT_MAX, 1+i });
                    return;
                }
            }

            table30_.unselect();
        }
    }

    void on_name_activate(const tau::ustring & name, int place) {
        auto & sect = kstate_.section(tau::str_format(place));
        kstate_.set_string("name", name);

        for (int n = 29; n > place; --n) {
            auto & sect1 = kstate_.section(tau::str_format(n-1));
            auto & sect2 = kstate_.section(tau::str_format(n));
            int nn = kstate_.get_integer(sect1, "score");
            tau::ustring nname = kstate_.get_string(sect1, "name", tau::login_name());
            kstate_.set_integer(sect2, "score", nn);
            kstate_.set_string(sect2, "name", nname);
            name30_[n].assign(nname);
            score30_[n].assign(nn);
        }

        kstate_.set_string(sect, "name", name);
        kstate_.set_integer(sect, "score", score_);
        score30_[place].assign(score_);
        name30_[place].disallow_edit();
        table30_.select({ INT_MIN, place, INT_MAX, 1+place });
        if (50000 <= kstate_.get_integer(kstate_.section("29"), "score")) { reset_button_.show(); }
    }

    void on_name_cancel(int place) {
        table30_.unselect();
        name30_[place].disallow_edit();

        for (int i = place; i < 30; ++i) {
            auto & sect = kstate_.section(tau::str_format(i));
            name30_[i].assign(kstate_.get_string(sect, "name", tau::login_name()));
            score30_[i].assign(kstate_.get_integer(sect, "score"));
        }
    }

    // Figures:
    // 0. x
    // 1. xx
    // 2. xxx
    // 3. x
    //    x
    // 4. x
    //    x
    //    x
    // 5. xx
    //     x
    // 6. xx
    //    x
    // 7. x
    //    xx
    // 8.  x
    //    xx
    bool new_fig() {
        std::uniform_int_distribution<std::mt19937::result_type> dist(0, 9);
        int fig = dist(rng_);

        switch (fig) {
            case 1:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                if (!alloc(1, 0, 1+XMAX/2, gen())) { return false; }
                return true;

            case 2:
                if (!alloc(0, 0, XMAX/4, gen())) { return false; }
                if (!alloc(1, 0, 1+XMAX/4, gen())) { return false; }
                if (!alloc(2, 0, 2+XMAX/4, gen())) { return false; }
                return true;

            case 3:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                if (!alloc(1, 1, XMAX/2, gen())) { return false; }
                return true;

            case 4:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                if (!alloc(1, 1, XMAX/2, gen())) { return false; }
                if (!alloc(2, 2, XMAX/2, gen())) { return false; }
                return true;

            case 5:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                if (!alloc(1, 0, 1+XMAX/2, gen())) { return false; }
                if (!alloc(2, 1, 1+XMAX/2, gen())) { return false; }
                return true;

            case 6:
                if (!alloc(0, 1, XMAX/2, gen())) { return false; }
                if (!alloc(1, 0, XMAX/2, gen())) { return false; }
                if (!alloc(2, 0, 1+XMAX/2, gen())) { return false; }
                return true;

            case 7:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                if (!alloc(1, 1, XMAX/2, gen())) { return false; }
                if (!alloc(2, 1, 1+XMAX/2, gen())) { return false; }
                return true;

            case 8:
                if (!alloc(0, 1, XMAX/2, gen())) { return false; }
                if (!alloc(1, 1, 1+XMAX/2, gen())) { return false; }
                if (!alloc(2, 0, 1+XMAX/2, gen())) { return false; }
                return true;

            default:
                if (!alloc(0, 0, XMAX/2, gen())) { return false; }
                return true;
        }
    }

    void clear_fig() {
        for (int i = 0; i < 3; ++i) {
            if (-1 != fig_[i].x() && -1 != fig_[i].y()) {
                clear(fig_[i].y(), fig_[i].x());
            }
        }
    }

    bool alloc(int n, int y, int x, int c) {
        if (x < 0 || x > XMAX || y < 0 || y > YMAX || n < 0 || n > 3 || chrome_[y][x] >= 0) { return false; }
        tau::Color color;
        if (c >= 0 && c < sizeof colors_/sizeof *colors_) { color = colors_[c]; }
        chrome_[y][x] = c;
        boxes_[y][x].conf().brush(tau::Conf::BACKGROUND) = tau::Brush(color);
        fig_[n].set(x, y);
        return true;
    }

    void clear(int y, int x) {
        if (x >= 0 && x <= XMAX && y >= 0 && y <= YMAX) {
            chrome_[y][x] = -1;
            boxes_[y][x].conf().set(tau::Conf::BACKGROUND, mt_);

            for (int i = 0; i < 3; ++i) {
                if (fig_[i].x() == x && fig_[i].y() == y) {
                    fig_[i].set(-1, -1);
                    break;
                }
            }
        }
    }

    bool busy(const tau::Point * pts) const {
        bool cont;

        for (int i = 0; i < 3; ++i) {
            if (-1 != pts[i].x() && -1 != pts[i].y()) {
                cont = false;

                if (pts[i].x() < 0 || pts[i].x() > XMAX || pts[i].y() < 0 || pts[i].y() > YMAX) {
                    return true;
                }

                for (int j = 0; j < 3; ++j) {
                    if (-1 != fig_[j].x() && -1 != fig_[j].y() && fig_[j].x() == pts[i].x() && fig_[j].y() == pts[i].y()) {
                        cont = true;
                        break;
                    }
                }

                if (!cont) {
                    if (chrome_[pts[i].y()][pts[i].x()] >= 0) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    int gen() {
        std::uniform_int_distribution<std::mt19937::result_type> dist(0, (sizeof colors_/sizeof *colors_)-1);
        return dist(rng_);
    }

    void on_left() {
        int xmin = INT_MAX;
        tau::Point fig[3] { fig_[0], fig_[1], fig_[2] };
        int c[3] { -1, -1, -1 };
        int n = 0;

        for (int i = 0; i < 3; ++i) {
            if (-1 != fig[i].x() && -1 != fig[i].y()) {
                xmin = std::min(xmin, fig[i].x());
                c[n++] = chrome_[fig[i].y()][fig[i].x()];
            }
        }

        if (INT_MAX != xmin && 0 != xmin) {
            for (int i = 0; i < 3; ++i) {
                if (-1 != fig[i].x() && -1 != fig[i].y()) {
                    fig[i].translate(-1, 0);
                }
            }

            if (busy(fig)) { return; }
            clear_fig();
            n = 0;

            for (int i = 0; i < 3; ++i) {
                if (-1 != fig[i].x() && -1 != fig[i].y()) {
                    alloc(n, fig[i].y(), fig[i].x(), c[n]);
                    n++;
                }
            }
        }
    }

    void on_right() {
        int xmax = INT_MIN;
        tau::Point fig[3] { fig_[0], fig_[1], fig_[2] };
        int c[3] { -1, -1, -1 };
        int n = 0;

        for (int i = 0; i < 3; ++i) {
            if (-1 != fig[i].x() && -1 != fig[i].y()) {
                xmax = std::max(xmax, fig[i].x());
                c[n++] = chrome_[fig[i].y()][fig[i].x()];
            }
        }

        if (INT_MAX != xmax && XMAX != xmax) {
            for (int i = 0; i < 3; ++i) {
                if (-1 != fig[i].x() && -1 != fig[i].y()) {
                    fig[i].translate(1, 0);
                }
            }

            if (busy(fig)) { return; }
            clear_fig();
            n = 0;

            for (int i = 0; i < 3; ++i) {
                if (-1 != fig[i].x() && -1 != fig[i].y()) {
                    alloc(n, fig[i].y(), fig[i].x(), c[n]);
                    n++;
                }
            }
        }
    }

    void on_rot() {
        int xmin = INT_MAX, xmax = INT_MIN, ymax = INT_MIN;
        tau::Point fig[3] { fig_[0], fig_[1], fig_[2] };
        tau::Point ofig[3]  { tau::Point(-1, -1), tau::Point(-1, -1), tau::Point(-1, -1) };
        int c[3] { -1, -1, -1 };
        int n = 0;

        for (int i = 0; i < 3; ++i) {
            if (-1 != fig[i].x() && -1 != fig[i].y()) {
                xmin = std::min(xmin, fig[i].x());
                xmax = std::max(xmax, fig[i].x());
                ymax = std::max(ymax, fig[i].y());
                c[n++] = chrome_[fig[i].y()][fig[i].x()];
            }
        }

        if (2 == n) {
            if (fig[0].y() == fig[1].y()) {     // Horizontal xx.
                if (fig[0].x() == xmin) {
                    ofig[0] = fig[0];
                    ofig[1].set(fig[0].x(), fig[0].y()+1);
                }

                else if (ymax > 0) {
                    ofig[0] = fig[0];
                    ofig[1].set(fig[0].x(), fig[0].y()-1);
                }
            }

            else {                              // Vertical xx.
                if (fig[0].y() == ymax) {
                    if (fig[0].x() < XMAX) {
                        ofig[0] = fig[0];
                        ofig[1].set(fig[0].x()+1, fig[0].y());
                    }
                }

                else if (fig[0].x() > 0) {
                    ofig[0] = fig[0];
                    ofig[1].set(fig[0].x()-1, fig[0].y());
                }
            }
        }

        else if (3 == n) {
            // Horizontal xxx.
            if (fig[0].y() == fig[1].y() && fig[1].y() == fig[2].y()) {
                if (ymax < YMAX) {
                    if (0 == ymax) { ++ymax; }
                    ofig[0].set(fig[1].x(), fig[0].x() < fig[2].x() ? ymax-1 : ymax+1);
                    ofig[1].set(fig[1].x(), ymax);
                    ofig[2].set(fig[1].x(), fig[0].x() < fig[2].x() ? ymax+1 : ymax-1);
                }
            }

            // Vertical xxx.
            else if (fig[0].x() == fig[1].x() && fig[1].x() == fig[2].x()) {
                if (xmin > 0 && xmax < XMAX) {
                    ofig[0].set(fig[0].y() < fig[2].y() ? fig[1].x()+1 : fig[1].x()-1, fig[1].y());
                    ofig[1] = fig[1];
                    ofig[2].set(fig[0].y() < fig[2].y() ? fig[1].x()-1 : fig[1].x()+1, fig[1].y());
                }
            }

            // 01
            //  2
            else if (fig[0].x() < fig[1].x() && fig[1].x() == fig[2].x() && fig[0].y() < fig[2].y()) {
                if (fig[1].y() > 0) {
                    ofig[2] = fig[0];
                    ofig[0].set(fig[1].x(), fig[1].y()-1);
                    ofig[1] = fig[1];
                }
            }

            // 21
            //  0
            else if (fig[2].x() < fig[1].x() && fig[1].x() == fig[0].x() && fig[2].y() < fig[0].y()) {
                if (fig[1].y() > 0) {
                    ofig[0] = fig[2];
                    ofig[2].set(fig[1].x(), fig[1].y()-1);
                    ofig[1] = fig[1];
                }
            }

            //  0
            // 21
            else if (fig[2].x() < fig[1].x() && fig[1].x() == fig[0].x() && fig[0].y() < fig[2].y()) {
                if (fig[0].x() < XMAX) {
                    ofig[2] = fig[0];
                    ofig[0].set(fig[1].x()+1, fig[1].y());
                    ofig[1] = fig[1];
                }
            }

            //  2
            // 01
            else if (fig[0].x() < fig[1].x() && fig[1].x() == fig[2].x() && fig[2].y() < fig[0].y()) {
                if (fig[2].x() < XMAX) {
                    ofig[0] = fig[2];
                    ofig[2].set(fig[1].x()+1, fig[1].y());
                    ofig[1] = fig[1];
                }
            }

            // 10
            // 2
            else if (fig[0].x() > fig[1].x() && fig[1].x() == fig[2].x() && fig[2].y() > fig[0].y()) {
                if (fig[2].x() > 0) {
                    ofig[0] = fig[2];
                    ofig[2].set(fig[1].x()-1, fig[1].y());
                    ofig[1] = fig[1];
                }
            }

            // 12
            // 0
            else if (fig[2].x() > fig[1].x() && fig[1].x() == fig[0].x() && fig[0].y() > fig[2].y()) {
                if (fig[0].x() > 0) {
                    ofig[2] = fig[0];
                    ofig[0].set(fig[1].x()-1, fig[1].y());
                    ofig[1] = fig[1];
                }
            }

            // 0
            // 12
            else if (fig[2].x() > fig[1].x() && fig[1].x() == fig[0].x() && fig[0].y() < fig[2].y()) {
                if (fig[2].y() < YMAX) {
                    ofig[0] = fig[2];
                    ofig[2].set(fig[1].x(), fig[1].y()+1);
                    ofig[1] = fig[1];
                }
            }

            // 2
            // 10
            else if (fig[0].x() > fig[1].x() && fig[1].x() == fig[2].x() && fig[2].y() < fig[0].y()) {
                if (fig[0].y() < YMAX) {
                    ofig[2] = fig[0];
                    ofig[0].set(fig[1].x(), fig[1].y()+1);
                    ofig[1] = fig[1];
                }
            }
        }

        if (-1 != ofig[0].x()) {
            if (busy(ofig)) { return; }
            clear_fig();
            n = 0;

            for (int i = 0; i < 3; ++i) {
                if (-1 != ofig[i].x() && -1 != ofig[i].y()) {
                    alloc(n, ofig[i].y(), ofig[i].x(), c[n]);
                    n++;
                }
            }
        }
    }

    void game_over() {
        down_cx_.block();
        wheel_cx_.block();
        key_cx_.unblock();
        pause_cx_.block();
        level_ = 0;
        clear_fig();
        tick_timer_.stop();
        level_timer_.stop();
        drop_timer_.stop();
        pool_timer_.stop();
        bonus_timer_.stop();
        pool_.clear();
        banner_.conf().set(tau::Conf::FOREGROUND, "Lime");
        banner_.assign("GAME\nOVER");
        bonus_value_.clear();
        left_action_.disable();
        right_action_.disable();
        drop_action_.disable();
        rot_action_.disable();
        pause_action_.disable();
        auto sel = table30_.selection();
        table30_.unselect();

        if (score_ > 0 && sel.ymin >= 0 && sel.ymin < 30) {
            for (int i = 29; i > sel.ymin; --i) { name30_[i].assign(name30_[i-1].wstr()), score30_[i].assign(score30_[i-1].value()); }
            if (kstate_.has_key("name")) { name30_[sel.ymin].assign(kstate_.get_string("name")); }
            scroller30_.pan(name30_[sel.ymin]);
            name30_[sel.ymin].allow_edit();
            name30_[sel.ymin].take_focus();
            name30_[sel.ymin].move_to(UINT_MAX);
            name30_[sel.ymin].select_all();
            score30_[sel.ymin].assign(score_);
            blink_cx_ = tau::Loop().this_loop().alarm(bind_back(fun(this, &Main::on_blink), std::ref(enter_.conf())), 556, true);
            name30_[sel.ymin].signal_focus_out().connect(fun(blink_cx_, &tau::connection::drop));
            name30_[sel.ymin].signal_focus_out().connect(fun(enter_, &tau::Widget::hide));
            enter_.show();
        }
    }

    void on_blink(tau::Conf & cnf) {
        if (cnf.item(tau::Conf::BACKGROUND).is_set()) { cnf.unset(tau::Conf::BACKGROUND); }
        else { cnf.set(tau::Conf::BACKGROUND, "#77A08C"); }
    }

    void new_game() {
        if (0 == level_) {
            if (!banner_.has_parent()) {
                box3_.remove_back();
                tau::Box board(tau::Orientation::SOUTH, 16);
                board.conf().font().resize(22);
                board.conf().font(tau::Conf::EDIT_FONT).resize(22);
                board.conf().font().make_bold();
                board.conf().font(tau::Conf::EDIT_FONT).make_bold();
                box3_.append(board);
                board.append(banner_);
                board.append(bonus_value_);
                level_label_.hint_margin_top(16);
                level_label_.conf().set(tau::Conf::FOREGROUND, "DodgerBlue");
                board.append(level_label_);
                level_value_.conf().set(tau::Conf::FOREGROUND, "Yellow");
                level_value_.hint_margin_bottom(16);
                board.append(level_value_);
                score_label_.conf().set(tau::Conf::FOREGROUND, "DodgerBlue");
                board.append(score_label_);
                board.append(score_value_);
            }

            bonus_value_.clear();
            table30_.unselect();
            reset_button_.hide();
            clear_fig();

            for (int y = 0; y < HMAX; ++y) {
                for (int x = 0; x < WMAX; ++x) {
                    clear(y, x);
                }
            }

            for (int i = 0; i < 30; ++i) {
                if (0 == kstate_.get_integer(kstate_.section(tau::str_format(i)), "score")) {
                    table30_.select({ INT_MIN, i, INT_MAX, 1+i });
                    break;
                }
            }

            level_ = 1;
            level_value_.assign(level_);
            score_ = 0;
            score_value_.assign(0);
            key_cx_.block();
            banner_.clear();
            left_action_.enable();
            right_action_.enable();
            drop_action_.enable();
            rot_action_.enable();
            pause_action_.enable();
            down_cx_.unblock();
            wheel_cx_.unblock();
            new_fig();
            level_timer_.start(60000, true);
            tick_timer_.start(860);
        }
    }

    bool on_tick() {
        tau::Point fig[3] { fig_[0], fig_[1], fig_[2] };
        tau::Point ofig[3] { tau::Point(-1, -1), tau::Point(-1, -1), tau::Point(-1, -1) };
        int c[3] { -1, -1, -1 };
        int n = 0;
        bool res = false;

        for (int i = 0; i < 3; ++i) {
            if (-1 != fig[i].x() && -1 != fig[i].y()) {
                c[n++] = chrome_[fig[i].y()][fig[i].x()];
                ofig[i].set(fig[i].x(), fig[i].y()+1);
            }
        }

        if (0 != n && !busy(ofig)) {
            if (-1 != ofig[0].x()) {
                clear_fig();
                n = 0;

                for (int i = 0; i < 3; ++i) {
                    if (-1 != ofig[i].x() && -1 != ofig[i].y()) {
                        alloc(n, ofig[i].y(), ofig[i].x(), c[n]);
                        n++;
                    }
                }

                res = true;
            }
        }

        return res;
    }

    int sweep() {
        int nb = 0, nnb, nc;

        for (int y = 0; y < HMAX; ++y) {
            for (int x = 0; x < WMAX; ++x) {
                if (chrome_[y][x] >= 0) {
                    int x1, x2, y1, y2, ymin;

                    // Horizontal sweep.
                    for (x1 = x-1; x1 >= 0 && chrome_[y][x1] == chrome_[y][x]; --x1);
                    for (x2 = x+1; x2 < WMAX && chrome_[y][x2] == chrome_[y][x]; ++x2);

                    if (x2-x1 > 3) {
                        nc = x2-x1;
                        nnb = 0;

                        for (++x1; x1 < x2; ++x1) {
                            if (0 == pool_[x1].count(y)) {
                                pool_[x1].insert(y);
                                boxes_[y][x1].conf().set(tau::Conf::BACKGROUND, "Navy");
                                nnb = 1;
                            }
                        }

                        if (0 != nnb) { h_branches_ += nnb; score(50*(nc-3)+10*(level_-1)+(YMAX-y)*3); }
                        nb += nnb;
                    }

                    // Vertical sweep.
                    for (y1 = y-1; y1 >= 0 && chrome_[y1][x] == chrome_[y][x]; --y1);
                    for (y2 = y+1; y2 < HMAX && chrome_[y2][x] == chrome_[y][x]; ++y2);

                    if (y2-y1 > 3) {
                        nc = y2-y1;
                        ymin = y1+1;
                        nnb = 0;

                        for (++y1; y1 < y2; ++y1) {
                            if (0 == pool_[x].count(y1)) {
                                pool_[x].insert(y1);
                                boxes_[y1][x].conf().set(tau::Conf::BACKGROUND, "Navy");
                                nnb = 1;
                            }
                        }

                        if (0 != nnb) { v_branches_ += nnb; score(40*(nc-3)+8*(level_-1)+(YMAX-ymin)*3); }
                        nb += nnb;
                    }

                    // Diagonal sweep, descend.
                    for (y1 = y-1, x1 = x-1; y1 >= 0 && x1 >= 0 && chrome_[y1][x1] == chrome_[y][x]; --y1, --x1);
                    for (y2 = y+1, x2 = x+1; y2 < HMAX && x2 < WMAX && chrome_[y2][x2] == chrome_[y][x]; ++y2, ++x2);

                    if (y2-y1 > 3 && x2-x1 > 3) {
                        ymin = y1+1;
                        nnb = 0;
                        nc = std::max(y2-y1, x2-y1);

                        for (++y1, ++x1; y1 < y2 && x1 < x2; ++y1, ++x1) {
                            if (0 == pool_[x1].count(y1)) {
                                pool_[x1].insert(y1);
                                boxes_[y1][x1].conf().set(tau::Conf::BACKGROUND, "Navy");
                                nnb = 1;
                            }
                        }

                        if (0 != nnb) { d_branches_ += nnb; score(75*(nc-3)+15*(level_-1)+(YMAX-ymin)*3); }
                        nb += nnb;
                    }

                    // Diagonal sweep, ascend.
                    for (y1 = y-1, x2 = x+1; y1 >= 0 && x2 < WMAX && chrome_[y1][x2] == chrome_[y][x]; --y1, ++x2);
                    for (y2 = y+1, x1 = x-1; y2 < HMAX && x1 >= 0 && chrome_[y2][x1] == chrome_[y][x]; ++y2, --x1);

                    if (y2-y1 > 3 && x2-x1 > 3) {
                        ymin = y1+1;
                        nnb = 0;
                        nc = std::max(y2-y1, x2-y1);

                        for (++y1, --x2; y1 < y2 && x1 < x2; ++y1, --x2) {
                            if (0 == pool_[x2].count(y1)) {
                                pool_[x2].insert(y1);
                                boxes_[y1][x2].conf().set(tau::Conf::BACKGROUND, "Navy");
                                nnb = 1;
                            }
                        }

                        if (0 != nnb) { d_branches_ += nnb; score(75*(nc-3)+15*(level_-1)+(YMAX-ymin)*3); }
                        nb += nnb;
                    }
                }
            }
        }

        if (0 != nb) { pool_timer_.start(200); }
        return nb;
    }

    void on_pool_timer() {
        for (auto & p: pool_) {
            for (int y: p.second) {
                for (; y > 0 && chrome_[y][p.first] >= 0; --y) { chrome_[y][p.first] = chrome_[y-1][p.first]; }
                chrome_[y][p.first] = -1;
            }

            for (int y = 0; y < HMAX; ++y) {
                boxes_[y][p.first].conf().set(tau::Conf::BACKGROUND, chrome_[y][p.first] >= 0 ? colors_[chrome_[y][p.first]] : mt_);
            }
        }

        pool_.clear();
        int nb = sweep();

        if (0 == nb) {
            bool empty = true;

            for (int y = 0; y < HMAX; ++y) {
                for (int x = 0; x < WMAX; ++x) {
                    if (chrome_[y][x] >= 0) {
                        empty = false;
                        break;
                    }
                }
            }

            if (empty) {
                bonus_ += 1000;
            }

            if (v_branches_+h_branches_+d_branches_ > 1) {
                if (d_branches_) {
                    if (v_branches_) { --v_branches_; }
                    else if (h_branches_) { --h_branches_; }
                    else { --d_branches_; }
                }

                else if (h_branches_) {
                    if (v_branches_) { --v_branches_; }
                    else { --h_branches_; }
                }

                else {
                    --v_branches_;
                }

                bonus_ += v_branches_*400;
                bonus_ += h_branches_*500;
                bonus_ += d_branches_*750;
            }

            if (0 != bonus_) {
                score(bonus_);
                bonus_value_.assign(bonus_);
                nbonus_ = 0;
                bonus_timer_.start(300, true);
                banner_.conf().set(tau::Conf::FOREGROUND, "Lime");
                banner_.assign("BONUS");
            }

            h_branches_ = v_branches_ = d_branches_ = 0;
            tick_timer_.restart();
            level_timer_.resume();
            if (!new_fig()) { game_over(); }
        }
    }

    void on_tick_timer() {
        if (!on_tick()) {
            score(2*(level_-1)-1);
            fig_[0] = fig_[1] = fig_[2] = tau::Point(-1, -1);
            int nb = sweep();

            if (0 != nb) {
                bonus_ = 0;
                tick_timer_.pause();
                level_timer_.pause();
                return;
            }

            if (!new_fig()) {
                game_over();
                return;
            }
        }

        tick_timer_.restart();
    }

    void on_drop_timer() {
        if (!on_tick()) {
            tick_timer_.restart();
            level_timer_.resume();
        }

        else {
            drop_timer_.start(50);
        }
    }

    void on_bonus_timer() {
        if (++nbonus_ >= 6) {
            if (pause_cx_.blocked()) { banner_.clear(); }
            bonus_value_.clear();
            bonus_timer_.stop();
        }

        else {
            if (bonus_value_.empty()) { bonus_value_.assign(); }
            else { bonus_value_.clear(); }
        }
    }

    void on_drop() {
        level_timer_.pause();
        tick_timer_.pause();
        drop_timer_.start(50);
    }

    void on_level_timer() {
        int to = tick_timer_.timeout();

        if (to >= 20) {
            tick_timer_.restart(6*to/7);
            level_value_.assign(++level_);
        }
    }

    void on_pause() {
        if (pause_cx_.blocked()) {
            while (pause_cx_.blocked()) { pause_cx_.unblock(); }
            level_timer_.pause();
            tick_timer_.pause();
            drop_timer_.pause();
            pool_timer_.pause();
            banner_.conf().set(tau::Conf::FOREGROUND, "Red");
            banner_.assign("PAUSE");
            escape_action_.disable();
            left_action_.disable();
            right_action_.disable();
            drop_action_.disable();
            rot_action_.disable();
            down_cx_.block();
            wheel_cx_.block();
        }

        else {
            pause_cx_.block();
            tick_timer_.restart();
            level_timer_.resume();
            drop_timer_.resume();
            pool_timer_.resume();
            banner_.clear();
            escape_action_.enable();
            left_action_.enable();
            right_action_.enable();
            drop_action_.enable();
            rot_action_.enable();
            down_cx_.unblock();
            wheel_cx_.unblock();
        }
    }

    bool on_mouse_down(int mbt, int mm, const tau::Point & pt) {
        if (tau::MBT_LEFT == mbt) { on_rot(); }
        else { on_drop(); }
        return true;
    }

    bool on_mouse_wheel(int delta, int mm, const tau::Point & pt) {
        if (delta < 0) { on_left(); }
        else { on_right(); }
        return true;
    }

    bool on_key_pause(int kc, int km) {
        on_pause();
        return true;
    }

    bool on_key_down(int kc, int km) {
        if (!tau::key_code_is_modifier(kc)) {
            new_game();
            return true;
        }

        return false;
    }

    void on_escape() {
        if (0 != level_) { table30_.unselect(); game_over(); }
        else { close(); }
    }

    void on_table_size() {
        tau::Size size = table_.size();
        unsigned s = (size.height()-HMAX-2)/HMAX;

        for (int y = 0; y < HMAX; ++y) {
            for (int x = 0; x < WMAX; ++x) {
                boxes_[y][x].hint_size(s);
            }
        }
    }

    void on_top_hints(tau::Hints) {
        unsigned s = table30_.required_size().height();
        hint_min_size(7*s/8);
    }

    void on_geometry_changed() {
        std::vector<intmax_t> v = { position().x(), position().y(), size().iwidth(), size().iheight() };
        kstate_.set_integers("geometry", v);
    }

    void on_reset() {
        const tau::ustring login = tau::login_name();
        std::multiset<tau::ustring> l;

        for (int i = 0; i < 30; ++i) {
            auto & sect = kstate_.section(tau::str_format(i));
            const tau::ustring s = kstate_.get_string(sect, "name");
            if (!s.empty() && 0 != kstate_.get_integer(sect, "score")) { l.insert(s); }
        }

        std::map<int, tau::ustring> rate;
        for (auto & s: l) { rate[l.count(s)] = s; }

        const tau::ustring name = rate.empty() ? login : rate.rbegin()->second;

        for (int i = 0; i < 30; ++i) {
            auto & sect = kstate_.section(tau::str_format(i));
            kstate_.set_string(sect, "name", name);
            name30_[i].assign(name);
            kstate_.set_integer(sect, "score", 0);
            score30_[i].assign(0);
        }

        kstate_.remove_key("name");
        reset_button_.hide();
        table30_.unselect();
    }

    bool on_table_paint(tau::Painter pr, const tau::Rect &) {
        if (auto sz = table_.size()) {
            auto e = boxes_[HMAX-1][WMAX-1].origin()+boxes_[HMAX-1][WMAX-1].size();
            pr.rectangle(0, 0, e.x(), e.y());
            pr.set_pen(tau::Color());
            pr.stroke();
        }

        return false;
    }

    bool on_box_paint(tau::Painter, const tau::Rect &, int y, int x) {
        if (x >= 0 && x < WMAX && y >= 0 && y < HMAX) {
            if (auto pr = table_.painter()) {
                auto o = boxes_[y][x].origin();
                auto s = boxes_[y][x].size();
                tau::Color black;

                if (chrome_[y][x] >= 0) {
                    pr.rectangle(o.x()-1, o.y()-1, o.x()+s.width(), o.y()+s.height());
                    pr.set_pen(black);
                    pr.stroke();
                }

                else {
                    if (y > 0) {         // top
                        pr.move_to(o.x()-(x > 0 ? 1 : 0), o.y()-1);
                        pr.line_to(o.x()+s.width(), o.y()-1);
                        pr.set_pen(chrome_[y-1][x] < 0 ? mt_ : black);
                        pr.stroke();
                    }

                    if (y < HMAX-1) {    // bottom
                        pr.move_to(o.x()-(x > 0 ? 1 : 0), o.y()+s.height());
                        pr.line_to(o.x()+s.width(), o.y()+s.height());
                        pr.set_pen(chrome_[y+1][x] < 0 ? mt_ : black);
                        pr.stroke();
                    }

                    if (x > 0) {         // left
                        pr.move_to(o.x()-1, o.y()-(y > 0 ? 1 : 0));
                        pr.line_to(o.x()-1, o.y()+s.height()+(y < HMAX-1 ? 0 : -1));
                        pr.set_pen(chrome_[y][x-1] < 0 ? mt_ : black);
                        pr.stroke();
                    }

                    if (x < WMAX-1) {    // right
                        pr.move_to(o.x()+s.width(), o.y()-(y > 0 ? 1 : 0));
                        pr.line_to(o.x()+s.width(), o.y()+s.height()+(y < HMAX-1 ? 0 : -1));
                        pr.set_pen(chrome_[y][x+1] < 0 ? mt_ : black);
                        pr.stroke();
                    }
                }
            }
        }

        return false;
    }
};

int main(int, char **) {
    try {
        tau::program_name("tetcolor");
        kstate_.create_from_file(tau::path_build(tau::path_user_config_dir(), "tetcolor.ini"));
        tau::Timer timer(fun(kstate_, &tau::Key_file::flush));
        kstate_.signal_changed().connect(bind_back(fun(timer, &tau::Timer::start), 6789, false));
        auto v = kstate_.get_integers("geometry", 4);
        Main wnd(TITLE, tau::Rect(v[0], v[1], tau::Size(v[2], v[3])));
        tau::Loop().run();
        kstate_.flush();
    }

    catch (tau::exception & x) { std::cerr << "** tau::exception thrown: " << x.what() << std::endl; }
    catch (std::exception & x) { std::cerr << "** std::exception thrown: " << x.what() << std::endl; }
    catch (...) { std::cerr << "** unknown exception thrown" << std::endl; }
    return 0;
}

//END
