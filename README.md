# The Simple Tetris-like Game.

The latest release is [**1.2**](https://gitlab.com/mrcashe/tetcolor/-/releases/1.2).

This game was originally written in Pascal by Sergey Sotnikov for MS-DOS in 1991. 
Millions of copies of the game went to homes and offices and the game became 
a real "time killer" for those who played it, dramatically reducing productivity 
and taking time for self-improvement.

<p float="left">
  <img src="sotnikov.png" width="30%" />
  <img src="original.png" width="30%" />
  <img src="new.png" width="30%" />
</p>

Some people claim that Sotnikov was a scientist, but this is not a fact. 
In any case, traces of the original author have been lost in time...

Despite the outward resemblance to the immortal creation of 
[Alexey Pajitnov, 1985](https://en.wikipedia.org/wiki/Tetris), 
TETCOLOR is not a 100% clone of TETRIS. 

This variation differs from the usual Tetris in that you need to combine falling blocks by colors. 
They disappear only when at least three elements of the same color line up on a line in any direction, 
including the diagonal.

The playing field consists of 18 rows of 7 cubes per row. 
Cubes of six colors in various combinations can form four types of figures: 
a single cube, two cubes, a line or corner figure of three cubes. Unlike Tetris, 
it is impossible to see which figure will fall next. The speed of the game with 
the passage of time (the level up timer works regardless of pausing the game) increases.

The top 30 results are saved. When your worst score (number 30) exceeds 50000, 
you will be allowed to reset all scores.

This project is a remake of the above game using my cross-platform GUI library for the
C++ programming language, which can be found [here](https://gitlab.com/mrcashe/tau).
There are remakes of the game for Windows and Android platforms, but I wanted 
to make a game that can run on Unix-like systems because my home system is Linux.

## Using Prebuilt Binaries
The program consists of just one executable file without the use of any package managers.
You can download already built binaries for all of the below platforms. These binaries
are statically linked against **libtau**.

- [Linux 64 x86_64, glibc-2.19](https://gitlab.com/-/project/51131688/uploads/32a9159a19f06ab8ca17bdb21c19af53/tetcolor-linux64)
- [FreeBSD-13.1, x86_64](https://gitlab.com/-/project/51131688/uploads/8e294cd0416791284c4a71894c87ec4d/tetcolor-freebsd13.1)
- [Windows 32 bit](https://gitlab.com/-/project/51131688/uploads/bb053c0cf7685488975dde86eb7c5fe2/tetcolor32.exe)
- [Windows 64 bit](https://gitlab.com/-/project/51131688/uploads/011706eeb9920dca9321e057ddf5c7a7/tetcolor64.exe)

## Build from Source
Build from source is possible on Linux and Free BSD hosts. The build system is [**GNU Make**](https://www.gnu.org/software/make/).
The Windows targets compiled using M cross-platform environment, [**MXE**](https://mxe.cc). 
However, I do not recommend you to cross-compile project using MXE, better use prebuilt binaries for **MS Windows** mentioned above.

### Requirements
You must have the development libraries installed from the manufacturer of the C++ compiler:
**libstdc++** for `gcc` on Linux and **libc++** for `clang++` on Free BSD. In addition, you
need [**GNU Make**](https://www.gnu.org/software/make/), BSD make won't work, so for build
on Free BSD do not forget to install `gmake` if it is not already installed:
```sh
[~]$ sudo pkg install gmake
```
...and use `gmake` command instead of `make`.

#### libtau

You will also need to download, build and install the [tau library](https://gitlab.com/mrcashe/tau)
exactly of version **0.7.1**. Do not clone **git** repository, better download and unpack any of the below archives:
[**zip**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.zip)
[**gzip**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.gz)
[**bzip2**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.bz2)

Do not forget to make **libtau** with *--enable-devel* configuration parameter:
```sh
[~]$ ./configure --enable-devel
[~]$ make
[~]$ sudo make install
```

If you plan to build *tetcolor* with statically linked **libtau**, specify also *--enable-static* option:
```sh
[~]$ ./configure --enable-devel --enable-static
[~]$ make
[~]$ sudo make install
```

But I don't see any reason for you to build the project using static linkage.

### Download
Download can be done from the **GitHub** site.

#### Clone from the git repository
Therefore, **git** should be installed on your system.
Note that, the downloaded this way code is not proven and may be even unable to work.

The link is: https://gitlab.com/mrcashe/tetcolor.git

Choose directory where to clone remote repository and type in the console:

```sh
[~]$ git clone https://gitlab.com/mrcashe/tetcolor.git
```

The directory named `tetcolor` will appear.

#### Download latest release
All of the archives listed below contain the same source code, so you can use any of them.

##### Download zip archive provided by GitLab
This is automatically generated [**archive**](https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.zip).
```sh
[~]$ wget https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.zip
[~]$ unzip tetcolor-1.2.zip
```
The directory named `tetcolor-1.2` will appear.

##### Download gzip tarball provided by GitLab
This is automatically generated [**tarball**](https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.tar.gz).
```sh
[~]$ wget https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.tar.gz
[~]$ tar xf tetcolor-1.2.tar.gz
```
The directory named `tetcolor-1.2` will appear.

##### Download bzip2 tarball provided by GitLab
This is automatically generated [**tarball**](https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.tar.bz2).
```sh
[~]$ wget https://gitlab.com/mrcashe/tetcolor/-/archive/1.2/tetcolor-1.2.tar.bz2
[~]$ tar xf tetcolor-1.2.tar.bz2
```
The directory named `tetcolor-1.2` will appear.

### Build

The build system is **GNU make**. No configure script is provided. The host system can be Linux or Free BSD.

Available targets are:

**all**
> Builds binary executable file for host system, and, optionally, cross compile for Windows 32 and 64 bit
if MinGW cross-compilers (MXE) found within the system search path, i.e. **$PATH** environment variable.
The executable files made with debug option enabled. Note that, the dependency information
does not tracked, so if **libtau** sources changed, you need to clear project manually
and build it again.

**clean**
> remove built targets and object files.

**strip**
> remove debug information from executable files.

As you can see the standard targets *install* and *uninstall* does not provided because
build process generates only single file for each supported platform and you can install it using standard 
operating system tools.
	
If you would like to build executable with statically linked **libtau**, specify make target 'static'
in the command line:

```sh
[~]$ make static
```

# Have a Nice Game!

