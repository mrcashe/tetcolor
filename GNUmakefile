# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2023,2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

tau_version 		:= 0.7
CXXFLAGS 		+= -g -O2 -std=c++20

# Determine C++ compiler existence.
cxx 			:= $(shell which $(CXX))

ifneq (,$(cxx))
targets 		+= tetcolor
CXXFLAGS 		+= $(shell pkg-config --cflags tau-$(tau_version))
ifneq (,$(findstring static,$(MAKECMDGOALS)))
LDFLAGS 		+= -static-libgcc -static-libstdc++ $(shell pkg-config --variable=a tau-$(tau_version))
else
LDFLAGS 		+= $(shell pkg-config --libs tau-$(tau_version))
endif
endif

# Autodetect MinGW 32 bit C++ compiler.
WIN32_TARGET 		:= i686-w64-mingw32

ifeq ($(WIN32_CXX),)
WIN32_CXXFLAGS	+= $(shell pkg-config --cflags tau-$(tau_version)-$(WIN32_TARGET) 2>/dev/null)
WIN32_LDFLAGS 	+= $(shell pkg-config --static --libs tau-$(tau_version)-$(WIN32_TARGET) 2>/dev/null)
WIN32_HOST	:= $(shell pkg-config --variable=host tau-$(tau_version)-$(WIN32_TARGET) 2>/dev/null)
WIN32_CXX	:= $(WIN32_HOST)-c++
endif

ifneq ($(WIN32_CXXFLAGS),)
targets 		+= tetcolor32.exe
endif

# Autodetect MinGW 64 bit C++ compiler.
WIN64_TARGET 		:= x86_64-w64-mingw32

ifeq ($(WIN64_CXX),)
WIN64_CXXFLAGS 	+= $(shell pkg-config --cflags tau-$(tau_version)-$(WIN64_TARGET) 2>/dev/null)
WIN64_LDFLAGS 	+= $(shell pkg-config --static --libs tau-$(tau_version)-$(WIN64_TARGET) 2>/dev/null)
WIN64_HOST 	:= $(shell pkg-config --variable=host tau-$(tau_version)-$(WIN64_TARGET) 2>/dev/null)
WIN64_CXX	:= $(WIN64_HOST)-c++
endif

ifneq ($(WIN64_CXXFLAGS),)
targets 		+= tetcolor64.exe
endif

icons 			:= $(addsuffix .hh,$(wildcard *.ico))

all:	$(targets)

static: $(targets)

clean:
	@rm -vrf *.o *.exe tetcolor

xz:
	@cd ../; \
	tar -cJf tetcolor-$(shell date +%Y%m%d).tar.xz \
	tetcolor/*.cc tetcolor/tetcolor.hh tetcolor/*.ico* tetcolor/*.rc tetcolor/*.png \
	tetcolor/GNUmakefile tetcolor/README* tetcolor/LICENSE* tetcolor/AUTHORS

.PHONY: clean xz strip xxd static

%.ico.hh: %.ico
	@if [ -z `which xxd` ]; then \
	    echo "** Error: you need to have 'xxd' utility installed!" 1>&2; \
	    exit 1; \
	fi; \
	xxd -i $< $@

tetcolor: tetcolor.cc tetcolor.hh $(icons)
	$(CXX) -o $@ $< $(CXXFLAGS) $(LDFLAGS)

tetcolor32.exe: tetcolor.cc tetcolor.hh $(icons) tetcolor32.res.o
	$(WIN32_CXX) -o $@ -static -mwindows $(CXXFLAGS) $(WIN32_CXXFLAGS) $< $(WIN32_LDFLAGS) tetcolor32.res.o

tetcolor32.res.o: tetcolor.rc tetcolor.hh
	$(WIN32_HOST)-windres $< -o $@

tetcolor64.exe: tetcolor.cc tetcolor.hh $(icons) tetcolor64.res.o
	$(WIN64_CXX) -o $@ -static -mwindows $(CXXFLAGS) $(WIN64_CXXFLAGS) $< $(WIN64_LDFLAGS) tetcolor64.res.o

tetcolor64.res.o: tetcolor.rc tetcolor.hh
	$(WIN64_HOST)-windres $< -o $@

strip:
	@if [ -e tetcolor ]; then strip tetcolor; fi
	@if [ -e tetcolor32.exe ]; then $(WIN32_HOST)-strip tetcolor32.exe; fi
	@if [ -e tetcolor64.exe ]; then $(WIN64_HOST)-strip tetcolor64.exe; fi

# EOF
